#!/usr/bin/perl
use warnings;
use strict;

# r2, for NZBGet >= 0.6.0-testing-r241

# Configure
my $RenameNzb = '0'; # 1 = true - nzb-name.nzb > archive-name.nzb_processed, 0 = false - foo.nzb > foo.nzb_processed
# overridden if the archive contains multipls nzbs
my $DeleteArchive = '1'; # 1 = true - deletes successfully extracted archive, 0 = false - foo.ext > foo.ext.processed
# overridden if there are any errors during processing

my $ok = 1;

#my $dir = $ARGV[0];
printf "arg1 = $ENV{'NZBNP_FILENAME'}\n";
if ($ENV{'NZBNP_FILENAME'} =~ m!(.+)/([^/]+)\.(zip|rar)$!i){
   &ExtractNzb ($1, $2, $3);
}

sub ExtractNzb {
	my ($dir, $arch_name, $arch_ext) = @_;
	my $count = 0;
	my $nzb_name;
	my $nzb_ext;
	my $new_name = $arch_name;
	$new_name =~ s!\.nzb$!!;
	
	if ($arch_ext =~ m!^rar$!i){
		open ARCHIVE, "-|", "unrar x -y \"$dir/$arch_name.$arch_ext\" \"$dir\"" or die $!;
	}
	elsif ($arch_ext =~ m!^zip$!i){
		open ARCHIVE, "-|", "unzip -jo \"$dir/$arch_name.$arch_ext\" -d \"$dir\"" or die $!;
	}
	
	while (<ARCHIVE>){ 
		if (m!([^/]+)\.(nzb)  (.+OK )?$!i){
			$nzb_name = $1;
			$nzb_ext  = $2;
			printf "[INFO] Extracted: $nzb_name.$nzb_ext\n";
			utime (undef, undef, "$dir/$nzb_name.$nzb_ext"); # touch
			&Save ($dir, $nzb_name, $nzb_ext, $nzb_name, 'nzb_processed');
			$count ++;
		}
		elsif (m!([^/]+\.[^\.]{3,4})  (.+OK )?$!){
			&Delete ($dir, $1);
		}
	}
	close ARCHIVE or warn $?;
	
	if ($count == 1 and $RenameNzb and $nzb_name ne $new_name){
		&Save ($dir, $nzb_name, 'nzb_processed', $new_name, 'nzb_processed');
	}
	
	if ($DeleteArchive and $nzb_name and $ok){
		&Delete ($dir, "$arch_name.$arch_ext");
	}
	elsif ($nzb_name) { 
		&Save ($dir, $arch_name, $arch_ext, $arch_name, "$arch_ext.processed");
	}
	else { printf "[ERROR] No nzbs were extracted from: $arch_name.$arch_ext\n"; } 
}

sub Delete {
	my ($dir, $file) = @_;
	if (unlink ("$dir/$file")){
		printf "[INFO] Deleted: $file\n";
	}
	else { 
		$ok = 0; 
		printf "[ERROR] Couldn't delete: $file\n"; 
	}

}

sub Save {
	my ($dir, $name, $ext, $new_name, $new_ext) = @_; # $new_name can = $name
	if (rename("$dir/$name.$ext", "$dir/$new_name.$new_ext")){
		printf "[INFO] Renamed:  $name.$ext, $new_name.$new_ext\n";
	}
	else { 
		$ok = 0; 
		printf "[ERROR] Couldn't rename: $name.$ext, $new_name.$new_ext\n"; 
	}
}